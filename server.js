const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(__dirname + '/dist/angular-exo'));

app.get('/*', (req, res) => {

  res.sendFile(path.join(__dirname + '/dist/angular-exo/index.html'));
});

app.listen(process.env.PORT || 8080);

console.log('server started');
