import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { SignComponent } from './pages/sign/sign.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
{
  path: '',
  component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
{
path: 'login',
component: LoginComponent
},
{
path: 'sign',
component: SignComponent
},
{
path: 'dashboard',
component: DashboardComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
