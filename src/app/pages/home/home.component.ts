import { Component, OnInit } from '@angular/core';
import {MainComponent} from '../../layout/main/main.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  option: string;
  constructor() {
    this.option = 'DEFAULT';
  }

  ngOnInit() {
  }

}
