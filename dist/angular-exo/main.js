(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/login/login.component */ "./src/app/pages/login/login.component.ts");
/* harmony import */ var _pages_sign_sign_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/sign/sign.component */ "./src/app/pages/sign/sign.component.ts");
/* harmony import */ var _pages_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/dashboard/dashboard.component */ "./src/app/pages/dashboard/dashboard.component.ts");
/* harmony import */ var _pages_home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/home/home.component */ "./src/app/pages/home/home.component.ts");







const routes = [
    {
        path: '',
        component: _pages_home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"]
    },
    {
        path: 'home',
        component: _pages_home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"]
    },
    {
        path: 'login',
        component: _pages_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]
    },
    {
        path: 'sign',
        component: _pages_sign_sign_component__WEBPACK_IMPORTED_MODULE_4__["SignComponent"]
    },
    {
        path: 'dashboard',
        component: _pages_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"]
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'angular-exo';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _layout_header_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./layout/header/header.component */ "./src/app/layout/header/header.component.ts");
/* harmony import */ var _layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./layout/footer/footer.component */ "./src/app/layout/footer/footer.component.ts");
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/login/login.component */ "./src/app/pages/login/login.component.ts");
/* harmony import */ var _layout_main_main_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./layout/main/main.component */ "./src/app/layout/main/main.component.ts");
/* harmony import */ var _pages_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/dashboard/dashboard.component */ "./src/app/pages/dashboard/dashboard.component.ts");
/* harmony import */ var _pages_sign_sign_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/sign/sign.component */ "./src/app/pages/sign/sign.component.ts");
/* harmony import */ var _pages_home_home_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/home/home.component */ "./src/app/pages/home/home.component.ts");












let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _layout_header_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"],
            _layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_6__["FooterComponent"],
            _pages_login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"],
            _layout_main_main_component__WEBPACK_IMPORTED_MODULE_8__["MainComponent"],
            _pages_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_9__["DashboardComponent"],
            _pages_sign_sign_component__WEBPACK_IMPORTED_MODULE_10__["SignComponent"],
            _pages_home_home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/layout/footer/footer.component.html":
/*!*****************************************************!*\
  !*** ./src/app/layout/footer/footer.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer>\n\n  <p>Copyright © Socializer 2019</p>\n\n</footer>\n"

/***/ }),

/***/ "./src/app/layout/footer/footer.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/layout/footer/footer.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FooterComponent = class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
};
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer',
        template: __webpack_require__(/*! ./footer.component.html */ "./src/app/layout/footer/footer.component.html"),
        styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/layout/footer/footer.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FooterComponent);



/***/ }),

/***/ "./src/app/layout/header/header.component.html":
/*!*****************************************************!*\
  !*** ./src/app/layout/header/header.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header *ngIf=\"option===options[0]\">\n\n  <div class=\"header-logo\">\n    <a [routerLink]=\"['']\" routerLinkActive=\"active\">\n      <img src=\"../../../assets/img/logo_socializer.svg\">\n    </a>\n  </div>\n\n  <div class=\"header-menu\">\n\n    <nav class=\"menu-wrapper\">\n\n      <ul class=\"nav\">\n\n        <li class=\"nav-item\">\n          <a href=\"#\" class=\"nav-link\">Why Socialize</a>\n        </li>\n        <li class=\"nav-item\">\n          <a href=\"#\" class=\"nav-link\">Features</a>\n        </li>\n        <li class=\"nav-item\">\n          <a [routerLink]=\"['/pricing']\" routerLinkActive=\"active\" class=\"nav-link\">Pricing</a>\n        </li>\n        <li class=\"nav-item\">\n          <a class=\"nav-link btn btn-rounded btn-transparent btn-border--blue\" [routerLink]=\"['/sign']\"\n            routerLinkActive=\"active\">Register</a>\n        </li>\n\n      </ul>\n\n      <div class=\"social-wrapper\">\n\n        <ul>\n          <li>\n            <a href=\"\" class=\"nav-item\">\n              <i class=\"fab fa-facebook-f\"></i>\n            </a>\n            <a href=\"\" class=\"nav-item\">\n              <i class=\"fab fa-instagram\"></i>\n            </a>\n            <a href=\"\" class=\"nav-item\">\n              <i class=\"fab fa-linkedin-in\"></i>\n            </a>\n          </li>\n        </ul>\n\n      </div>\n\n    </nav>\n\n  </div>\n\n</header>\n\n<header *ngIf=\"option===options[1]\">\n\n  <div class=\"header-logo\">\n    <a href=\"/\">\n      <img src=\"../../../assets/img/logo_socializer.svg\">\n    </a>\n  </div>\n  <div class=\"login-section py-2\">\n    Déjà inscrit ?\n    <a class=\"btn btn-rounded btn-transparent btn-border--blue\" [routerLink]=\"['/login']\" routerLinkActive=\"active\">connexion</a>\n  </div>\n</header>\n\n<header *ngIf=\"option===options[2]\">\n  <div class=\"header-logo\">\n    <a href=\"/\">\n      <img src=\"../../../assets/img/logo_socializer.svg\">\n    </a>\n  </div>\n  <!-- login section header -->\n  <div class=\"login-section\">\n    Vous n’avez pas encore de compte ?\n    <a class=\"btn btn-rounded btn-transparent btn-border--blue\" [routerLink]=\"['/sign']\" routerLinkActive=\"active\">inscription</a>\n  </div>\n</header>\n"

/***/ }),

/***/ "./src/app/layout/header/header.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/layout/header/header.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/header/header.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/header/header.component.ts ***!
  \***************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HeaderComponent = class HeaderComponent {
    constructor() {
        this.options = ['DEFAULT', 'SECOND', 'THIRD'];
    }
    ngOnInit() { }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], HeaderComponent.prototype, "option", void 0);
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: __webpack_require__(/*! ./header.component.html */ "./src/app/layout/header/header.component.html"),
        styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/layout/header/header.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HeaderComponent);



/***/ }),

/***/ "./src/app/layout/main/main.component.html":
/*!*************************************************!*\
  !*** ./src/app/layout/main/main.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<main>\n\n  <section class=\"home-section--hero container\">\n\n      <div class=\"left-part\">\n\n          <div class=\"hero-content\">\n\n              <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>\n              <h1>Socializer Insight</h1>\n              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil perspiciatis vero perferendis enim rerum, harum neque mollitia consequuntur dolor aspernatur accusamus modi. Quas eveniet tempora omnis soluta dolorum voluptatibus praesentium!</p>\n              <a href=\"\" class=\"btn btn-rounded btn-yellow\">Schedule a demo</a>\n\n          </div>\n\n      </div>\n\n      <div class=\"right-part\">\n\n          <img src=\"../../../assets/img/illustration.png\" alt=\"\">\n\n      </div>\n\n  </section>\n\n  <section class=\"home-section--overview container\">\n\n      <div class=\"main-title\">\n\n          <h2>Know your followers</h2>\n          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta velit commodi facilis.</p>\n\n      </div>\n      <div class=\"interface-wrapper py-5\">\n\n          <div class=\"interface-item\">\n\n              <div class=\"interface-thumbnail\">\n\n                  <img src=\"../../../assets/img/screen1.png\" alt=\"\">\n\n              </div>\n              <div class=\"interface-item--content\">\n\n                  <h3>Forecast</h3>\n                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laborum soluta porro repellat, neque quos! Nisi atque odio dolorum voluptatum inventore</p>\n\n              </div>\n\n          </div>\n          <div class=\"interface-item\">\n\n              <div class=\"interface-thumbnail\">\n\n                  <img src=\"../../../assets/img/screen2.png\" alt=\"\">\n\n              </div>\n              <div class=\"interface-item--content\">\n\n                  <h3>Visualization</h3>\n                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laborum soluta porro repellat, neque quos! Nisi atque odio dolorum voluptatum inventore</p>\n\n              </div>\n\n          </div>\n          <div class=\"interface-item\">\n\n              <div class=\"interface-thumbnail\">\n\n                  <img src=\"../../../assets/img/screen3.png\" alt=\"\">\n\n              </div>\n              <div class=\"interface-item--content\">\n\n                  <h3>Improvment</h3>\n                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laborum soluta porro repellat, neque quos! Nisi atque odio dolorum voluptatum inventore</p>\n\n              </div>\n\n          </div>\n\n      </div>\n\n  </section>\n\n  <section class=\"home-section--features container\">\n\n      <div class=\"features-wrapper py-5\">\n\n          <div class=\"feature-card\">\n\n              <div class=\"card-icons\">\n\n              </div>\n\n              <h4>Feature 1</h4>\n              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, quasi.</p>\n\n          </div>\n\n          <div class=\"feature-card\">\n\n              <div class=\"card-icons\">\n\n              </div>\n\n              <h4>Feature 2</h4>\n              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, quasi.</p>\n\n          </div>\n\n          <div class=\"feature-card\">\n\n              <div class=\"card-icons\">\n\n              </div>\n\n              <h4>Feature 3</h4>\n              <p>Sint ratione, vel doloribus quis minus quos provident. Excepturi, quasi.</p>\n\n          </div>\n\n          <div class=\"feature-card\">\n\n              <div class=\"card-icons\">\n\n              </div>\n\n              <h4>Feature 4</h4>\n              <p>Cupiditate dolorem, pariatur unde sequi laudantium deserunt quo animi sit modi iure sint ratione, vel doloribus quis minus quos provident.</p>\n\n          </div>\n\n          <div class=\"feature-card\">\n\n              <div class=\"card-icons\">\n\n              </div>\n\n              <h4>Feature 5</h4>\n              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, quasi.</p>\n\n          </div>\n\n          <div class=\"feature-card\">\n\n              <div class=\"card-icons\">\n\n              </div>\n\n              <h4>Feature 6</h4>\n              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate dolorem, pariatur unde sequi.</p>\n\n          </div>\n\n      </div>\n\n  </section>\n\n  <section class=\"home-section--demo bg-color--blue py-5\">\n\n      <div class=\"main-title\">\n\n          <h2>Want a personal Tour ?</h2>\n          <p>Dolores modi reprehenderit, facilis nihil nihil nam iste magni recusandae tempora a voluptatem.</p>\n          <div class=\"py-5\">\n\n              <a href=\"\" class=\"btn btn-square btn-green\">Schedule a demo</a>\n\n          </div>\n\n      </div>\n\n  </section>\n\n</main>\n"

/***/ }),

/***/ "./src/app/layout/main/main.component.scss":
/*!*************************************************!*\
  !*** ./src/app/layout/main/main.component.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tYWluL21haW4uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/main/main.component.ts":
/*!***********************************************!*\
  !*** ./src/app/layout/main/main.component.ts ***!
  \***********************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let MainComponent = class MainComponent {
    constructor() { }
    ngOnInit() {
    }
};
MainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-main',
        template: __webpack_require__(/*! ./main.component.html */ "./src/app/layout/main/main.component.html"),
        styles: [__webpack_require__(/*! ./main.component.scss */ "./src/app/layout/main/main.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], MainComponent);



/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.html":
/*!**********************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header [option]=\"option\"> </app-header>\n<main>\n\n  <div class=\"dashboard-container\">\n\n      <!-- sidebar -->\n      <aside id=\"sideBarUser\">\n\n          <!-- header sidebar -->\n          <div class=\"header-user\">\n\n              <div class=\"logo-wrapper\">\n              </div>\n              <div class=\"profil-picture no-picture\">\n                  <a href=\"#\">\n                      <div class=\"no-picture-wrapper\">\n                          <i class=\"fas fa-upload\"></i>\n                          <p>Ajouter une photo</p>\n                      </div>\n                  </a>\n              </div>\n              <h3>Daniel</h3>\n\n          </div>\n\n          <!-- menu -->\n          <nav>\n              <ul>\n                  <li>\n                      <a href=\"\">\n                          <i class=\"fas fa-camera-retro\"></i>\n                          monitoring\n                      </a>\n                  </li>\n                  <li>\n                      <a href=\"\">\n                          <i class=\"far fa-comments\"></i>\n                          messagerie\n                      </a>\n                  </li>\n                  <li class=\"active\">\n                      <a href=\"../dashboard-user/infos-user.php\">\n                          <i class=\"fas fa-user-circle\"></i>\n                          informations\n                      </a>\n                  </li>\n                  <li>\n                      <a href=\"../dashboard-user/prefs-user.php\">\n                          <i class=\"fas fa-cog\"></i>\n                          préférences\n                      </a>\n                  </li>\n              </ul>\n          </nav>\n\n          <div class=\"footer-user\">\n              <button type=\"button\" name=\"button\" class=\"btn btn-reset\">\n                  <i class=\"fas fa-power-off\"></i>\n                  déconnexion\n              </button>\n          </div>\n\n      </aside>\n\n      <!-- right content -->\n      <div id=\"mainContent\">\n\n          <!-- title dashboard -->\n          <div class=\"title-dashboard\">\n              <h2>Mes informations</h2>\n          </div>\n\n          <div class=\"button-group\">\n              <button type=\"button\" class=\"btn btn-transparent btn-border--blue\">\n                  <i class=\"fas fa-edit\"></i>\n                  modifier mes informations\n              </button>\n              <button type=\"button\" class=\"btn btn-green\">\n                  <i class=\"fas fa-camera-retro\"></i>\n                  modifier ma photo\n              </button>\n          </div>\n\n          <!-- infos users -->\n          <div class=\"user-info-wrapper\">\n\n              <div class=\"white-card mh\">\n                  <div class=\"info-user-line\">\n                      <label>Nom:</label>\n                      <span>Zerbib</span>\n                  </div>\n                  <div class=\"info-user-line\">\n                      <label>Prénom:</label>\n                      <span>Vincent</span>\n                  </div>\n              </div>\n\n              <div class=\"white-card mh\">\n                  <div class=\"info-user-line\">\n                      <label>Adresse: </label>\n                      <span>13 rue de la paix, 75012 Paris 12ème</span>\n                  </div>\n                  <div class=\"info-user-line\">\n                      <label>Téléphone: </label>\n                      <span>06 26 02 65 16</span>\n                  </div>\n                  <div class=\"info-user-line\">\n                      <label>Mail: </label>\n                      <span>vincentzerbib@gmail.com</span>\n                  </div>\n              </div>\n          </div>\n\n          <div class=\"py-5\">\n              <button type=\"button\" class=\"btn btn-squared btn-blue\" data-toggle=\"modal\" data-target=\"#modalPhoto\">\n                  <i class=\"fas fa-key\"></i>\n                  modifier mon mot de passe\n              </button>\n\n          </div>\n\n      </div>\n\n  </div>\n\n</main>\n"

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.component.ts ***!
  \********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let DashboardComponent = class DashboardComponent {
    constructor() {
        this.option = 'DEFAULT';
    }
    ngOnInit() {
    }
};
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/pages/dashboard/dashboard.component.html"),
        styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/pages/dashboard/dashboard.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], DashboardComponent);



/***/ }),

/***/ "./src/app/pages/home/home.component.html":
/*!************************************************!*\
  !*** ./src/app/pages/home/home.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header [option]=\"option\"> </app-header>\n<app-main></app-main>\n"

/***/ }),

/***/ "./src/app/pages/home/home.component.scss":
/*!************************************************!*\
  !*** ./src/app/pages/home/home.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/home/home.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() {
        this.option = 'DEFAULT';
    }
    ngOnInit() {
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! ./home.component.html */ "./src/app/pages/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/pages/home/home.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HomeComponent);



/***/ }),

/***/ "./src/app/pages/login/login.component.html":
/*!**************************************************!*\
  !*** ./src/app/pages/login/login.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header [option]=\"option\"> </app-header>\n<div class=\"register-user-section\">\n\n  <div class=\"main-title\">\n    <h1>Connectez-vous sur <span class=\"color-red\">Socializer.</span> </h1>\n  </div>\n  <form class=\"register-user-form w-75 mx-auto\">\n    <div class=\"row\">\n      <div class=\"col-md-6 col-xs-12\">\n        <div class=\"form-group\">\n          <label for=\"\">Utilisateur / mail</label>\n          <input type=\"text\" class=\"form-control\" required=\"required\" placeholder=\"Utilisateur ou adresse mail\">\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-6 col-xs-12\">\n        <div class=\"form-group\">\n          <label for=\"\">Mot de passe</label>\n          <input type=\"password\" class=\"form-control\" required=\"required\" placeholder=\"Mot de passe\">\n        </div>\n      </div>\n    </div>\n    <div class=\"remember-check\">\n      <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" checked=\"\">\n      <label for=\"remember_me\">Se souvenir de moi</label>\n    </div>\n    <input type=\"hidden\" id=\"fos_user_registration_form__token\" name=\"fos_user_registration_form[_token]\" value=\"Q0FAwlluzXShFp-gIJG8E66IMoO55LwoYPCRo12Ys_Y\">\n    <button type=\"submit\" class=\"btn btn-rounded btn-green\">\n      Connexion\n    </button>\n    <a class=\"request-link\" href=\"request-password.php\">J'ai oublié mon mot de passe</a>\n  </form>\n\n</div>\n"

/***/ }),

/***/ "./src/app/pages/login/login.component.scss":
/*!**************************************************!*\
  !*** ./src/app/pages/login/login.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/login/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let LoginComponent = class LoginComponent {
    constructor() {
        this.option = 'THIRD';
    }
    ngOnInit() { }
};
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! ./login.component.html */ "./src/app/pages/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/pages/login/login.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], LoginComponent);



/***/ }),

/***/ "./src/app/pages/sign/sign.component.html":
/*!************************************************!*\
  !*** ./src/app/pages/sign/sign.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header [option]=\"option\"> </app-header>\n<div class=\"register-user-section container\">\n\n  <!-- login section header -->\n\n\n  <div class=\"main-title\">\n    <h1>Inscrivez-vous sur <span class=\"color-red\">Socializer.</span></h1>\n    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n      magna aliqua.</p>\n  </div>\n\n  <form class=\"register-user-form\">\n    <div class=\"row\">\n      <div class=\"col-md-6 col-xs-12\">\n        <div class=\"form-group\">\n          <label for=\"\">Prénom</label>\n          <input type=\"text\" required=\"required\" placeholder=\"Prénom\" class=\"form-control\">\n        </div>\n      </div>\n      <div class=\"col-md-6 col-xs-12\">\n        <div class=\"form-group\">\n          <label for=\"\">Nom</label>\n          <input type=\"text\" required=\"required\" placeholder=\"Nom\" class=\"form-control\">\n        </div>\n      </div>\n      <div class=\"col-md-6 col-xs-12\">\n        <div class=\"form-group\">\n          <label for=\"\">Nom d'utilisateur</label>\n          <input type=\"text\" required=\"required\" maxlength=\"180\" pattern=\".{5,}\" placeholder=\"Nom d'utilisateur\" class=\"form-control\"\n            title=\"Au moins 5 caractéres\">\n        </div>\n      </div>\n      <div class=\"col-md-6 col-xs-12\">\n        <div class=\"form-group\">\n          <label for=\"\">Email</label>\n          <input type=\"email\" required=\"required\" placeholder=\"Email\" class=\"form-control\">\n        </div>\n      </div>\n      <div class=\"col-md-6 col-xs-12\">\n        <div class=\"form-group\">\n          <label for=\"\">Mot de passe</label>\n          <input type=\"password\" required=\"required\" placeholder=\"Mot de passe\" class=\"form-control\">\n          <p class=\"help-block\">\n            Au moins 6 caractéres, avec une majuscule, une minuscule et un chiffre\n          </p>\n        </div>\n      </div>\n      <div class=\"col-md-6 col-xs-12\">\n        <div class=\"form-group\">\n          <label for=\"\">Confirmation mot de passe</label>\n          <input type=\"password\" required=\"required\" placeholder=\"Confirmez votre mot de passe\" class=\"form-control\">\n        </div>\n      </div>\n      <div class=\"col-md-12 col-xs-12\">\n        <div class=\"cgu-wrapper\">\n          <input type=\"checkbox\" id=\"cgu\" name=\"cgu\" required=\"required\">\n          En vous inscrivant en tant qu'utilisateur, vous confirmez que vous acceptez les Conditions Générales du site\n          et déclarez être informé des obligations civiles et fiscales qui incombent aux photographes exerçant une\n          activité rémunérée.\n          <a target=\"_blank\" href=\"/cgu\">Lire les CGU</a>\n        </div>\n      </div>\n    </div>\n    <button type=\"submit\" class=\"btn btn-rounded btn-green\">\n      Je m'inscris\n    </button>\n  </form>\n\n</div>\n"

/***/ }),

/***/ "./src/app/pages/sign/sign.component.scss":
/*!************************************************!*\
  !*** ./src/app/pages/sign/sign.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NpZ24vc2lnbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/sign/sign.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/sign/sign.component.ts ***!
  \**********************************************/
/*! exports provided: SignComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignComponent", function() { return SignComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SignComponent = class SignComponent {
    constructor() {
        this.option = 'SECOND';
    }
    ngOnInit() { }
};
SignComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sign',
        template: __webpack_require__(/*! ./sign.component.html */ "./src/app/pages/sign/sign.component.html"),
        styles: [__webpack_require__(/*! ./sign.component.scss */ "./src/app/pages/sign/sign.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SignComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/SpartHunter/Apps-Devel/Course_Project/angular-exo/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map